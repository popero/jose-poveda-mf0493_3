package com.daw.filmlist;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.daw.filmlist.model.Film;

import java.util.ArrayList;

public class FilmAdapter extends ArrayAdapter<Film> {

    int layoutResourceId;
    Context context;
    ArrayList<Film> data;

    public FilmAdapter(Context context, int layoutResourceId, ArrayList<Film> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        row = inflater.inflate(layoutResourceId, parent, false);

        TextView tv_title = row.findViewById(R.id.tv_title);
        tv_title.setText(data.get(position).getTitle());
        TextView tv_rating = row.findViewById(R.id.tv_rating);
        tv_rating.setText(String.valueOf(data.get(position).getRating()));

        int ratingColor = data.get(position).getRating();

        if (ratingColor<=1){
            tv_rating.setTextColor(context.getResources().getColor(R.color.red));
            tv_title.setTextColor(context.getResources().getColor(R.color.red));
        }else if (ratingColor>=2 && ratingColor<=3){
            tv_rating.setTextColor(context.getResources().getColor(R.color.black));
            tv_title.setTextColor(context.getResources().getColor(R.color.black));
        }else {
            tv_rating.setTextColor(context.getResources().getColor(R.color.green));
            tv_title.setTextColor(context.getResources().getColor(R.color.green));
        }
        return row;
    }
}
