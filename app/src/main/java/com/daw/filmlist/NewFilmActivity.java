package com.daw.filmlist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.daw.filmlist.controller.FilmController;
import com.daw.filmlist.model.Film;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NewFilmActivity extends AppCompatActivity {

    EditText et_title, et_description, et_year, et_rating, et_url;
    FilmController controller;
    Film film;
    String id;
    Button btn_create;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_film);
        et_title = findViewById(R.id.et_title);
        et_description = findViewById(R.id.et_description);
        et_year = findViewById(R.id.et_year);
        et_rating = findViewById(R.id.et_rating);
        et_url = findViewById(R.id.et_url);
        btn_create = findViewById(R.id.btn_create);

        controller = FilmController.get(this);

        String id = getIntent().getStringExtra("idFilm");
        if (id != null) {
            film = controller.getFilm(id);
            showFilm();
        }
    }

    private void showFilm() {
        et_title.setText(film.getTitle());
        et_description.setText(film.getDescription());
        et_year.setText(String.valueOf(film.getYear()));
        et_rating.setText(String.valueOf(film.getRating()));
        et_url.setText(film.getUrl());
    }

    public void createPressed(View view) {
        String title = et_title.getText().toString();
        String description = et_description.getText().toString();
        String year = et_year.getText().toString();
        String rating = et_rating.getText().toString();
        String url = et_url.getText().toString();

        if (checkFields(title, description, year, rating, url)) {
            if (id != null) {
                film.setTitle(title);
                film.setDescription(description);
                film.setYear(Integer.parseInt(year));
                film.setRating(Integer.parseInt(rating));
                film.setUrl(url);
                controller.updateFilm(film);
            } else {
                Film film = new Film(title, description, Integer.parseInt(year), Integer.parseInt(rating), url);
                controller.createFilm(film);
            }
            finish();
        }
    }

    private boolean checkFields(String title, String description, String year, String rating, String url) {
        boolean result = true;
        String errorTitle=getString(R.string.error_title);
        String errorDescription=getString(R.string.error_description);
        String errorYear=getString(R.string.error_year);
        String errorRating=getString(R.string.error_rating);
        String errorUrl=getString(R.string.error_url);
        if ("".equals(title)) {
            result = false;
            et_title.setError(errorTitle);
        }
        if ("".equals(description)) {
            result = false;
            et_description.setError(errorDescription);
        }
        if ("".equals(year)) {
            result = false;
            et_year.setError(errorYear);
        }
        if ("".equals(rating)) {
            result = false;
            et_rating.setError(errorRating);
        }else {
            int r = Integer.parseInt(rating);
            if (r < 0 && r > 5){
                result = false;
                et_rating.setError(errorRating);
            }
        }
        if ("".equals(url)) {
            result = false;
            et_url.setError(errorUrl);
        }
        return result;
    }

    public static class RetrofitClientInstance {

        private static Retrofit retrofit;

        private static final String BASE_URL = "https://ghibliapi.herokuapp.com/";

        public static Retrofit getRetrofitInstance() {
            if (retrofit == null) {
                retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
            return retrofit;
        }
    }
}
