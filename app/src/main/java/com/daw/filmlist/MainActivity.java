package com.daw.filmlist;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.daw.filmlist.Retrofit.InspirationActivity;
import com.daw.filmlist.controller.FilmController;
import com.daw.filmlist.model.Film;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    ArrayList<Film> persons;
    FilmController controller;
    FilmAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.lv_list);
        persons = new ArrayList<Film>();
        controller = FilmController.get(this);
        adapter = new FilmAdapter(this, R.layout.row, persons);
        listView.setAdapter(adapter);

        SharedPreferences prefs;
        prefs = getSharedPreferences("Films", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        String email = prefs.getString("email", "");
        if ("".equals(email)) {
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
        }
        
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent (MainActivity.this, DetailActivity.class);
                intent.putExtra("idFilm", persons.get(position).getId());
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume(){
        super.onResume();
        showFilms();
    }

    private void showFilms(){
        persons.clear();
        persons.addAll(controller.getFilms());
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                Intent intent = new Intent(MainActivity.this, NewFilmActivity.class);
                startActivity(intent);
                return (true);

            case R.id.action_add1:
                Intent intent1 = new Intent(MainActivity.this, InspirationActivity.class);
                startActivity(intent1);
                return (true);
        }

        return (super.onOptionsItemSelected(item));
    }
}
