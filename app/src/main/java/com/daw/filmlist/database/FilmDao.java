package com.daw.filmlist.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.daw.filmlist.model.Film;

import java.util.List;

@Dao
public interface FilmDao {

    @Query("SELECT * FROM film")
    List<Film> getFilms();

    @Query("SELECT * FROM film where id like :uuid")
    Film getFilm(String uuid);

    @Insert
    void addFilm(Film film);

    @Delete
    void deleteFilm(Film film);

    @Update
    void updateFilm(Film film);
}
