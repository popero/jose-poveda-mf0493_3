package com.daw.filmlist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.daw.filmlist.controller.FilmController;
import com.daw.filmlist.model.Film;
import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {

    FilmController controller;
    Film film;
    TextView tv_title, tv_description, tv_year, tv_rating, tv_url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        String id = getIntent().getStringExtra("idFilm");
        controller = FilmController.get(this);
        film = controller.getFilm(id);

        tv_title = findViewById(R.id.tv_title);
        tv_description = findViewById(R.id.tv_description);
        tv_year = findViewById(R.id.tv_year);
        tv_rating = findViewById(R.id.tv_rating);
        tv_url = findViewById(R.id.tv_url);

        showPerson();
    }

    private void showPerson() {

        tv_title.setText(film.getTitle());
        tv_description.setText(film.getDescription());
        tv_year.setText(String.valueOf(film.getYear()));
        tv_rating.setText(String.valueOf(film.getRating()));
        tv_url.setText(film.getUrl());
        ImageView iv_image = findViewById(R.id.iv_image);

        Picasso.get().load(film.getUrl()).resize(350, 350).centerCrop().into(iv_image);
    }

    public void deletePressed(View view) {
        controller.deleteFilm(film);
        finish();
    }
}
