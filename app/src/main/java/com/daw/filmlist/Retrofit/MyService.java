package com.daw.filmlist.Retrofit;

import com.daw.filmlist.model.FilmG;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MyService {

    @GET("films/")
    Call<List<FilmG>> getFilms();
}
