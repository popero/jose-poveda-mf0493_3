package com.daw.filmlist.Retrofit;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.daw.filmlist.R;

import com.daw.filmlist.model.FilmG;

import java.util.ArrayList;

public class InspirationAdapter extends ArrayAdapter {

    int layoutResourceId;
    Context context;
    ArrayList<FilmG> data;

    public InspirationAdapter(Context context, int layoutResourceId, ArrayList<FilmG> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override

    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        row = inflater.inflate(layoutResourceId, parent, false);
        TextView tv_title1 = (TextView)row.findViewById(R.id.tv_title1);

        FilmG film = data.get(position);
        tv_title1.setText(film.getTitle());

        return row;
    }
}
