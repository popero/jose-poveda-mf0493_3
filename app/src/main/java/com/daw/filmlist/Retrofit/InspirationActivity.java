package com.daw.filmlist.Retrofit;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.daw.filmlist.R;
import com.daw.filmlist.model.FilmG;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InspirationActivity extends AppCompatActivity {

    ListView listView;
    InspirationAdapter adapter;
    ArrayList<FilmG> films;
    Activity activity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inspiration);
        listView = findViewById(R.id.lv_list1);
        films = new ArrayList<FilmG>();
        adapter = new InspirationAdapter(activity, R.layout.row_inspiration, films);
        listView.setAdapter(adapter);
    }

    private void getUsersFromRetrofit() {

        MyService service = RetrofitClientInstance.getRetrofitInstance().create(MyService.class);
        Call<List<FilmG>> call = service.getFilms();
        call.enqueue(new Callback<List<FilmG>>() {

            @Override

            public void onResponse(Call<List<FilmG>> call, Response<List<FilmG>> response) {
                films.addAll(response.body());
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<FilmG>> call, Throwable t) {

                Toast.makeText(activity, "ERROR", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected  void onResume() {
        super.onResume();
        getUsersFromRetrofit();
    }

}
