package com.daw.filmlist;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity {

    private String email, password;
    private TextView et_email, et_password, tv_urlFilms;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        tv_urlFilms = findViewById(R.id.tv_urlFilms);
        prefs = getSharedPreferences("Films", Context.MODE_PRIVATE);
    }

    public void starPressed(View view) {
        Intent intent = new Intent (Intent.ACTION_VIEW);
        intent.setData(Uri.parse( "https://www.abc.es/play/cine/peliculas/"));
        startActivity(intent);
    }

    public void createPressed(View view) {
        if ( checkEmail(email) && checkPassword(password)){
            email = et_email.getText().toString();
            password = et_password.getText().toString();
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("email", email );
            editor.commit();
            finish();
        }
}

    private boolean checkEmail(String email) {
        String errorString=getString(R.string.error_email);
        if ("".equals(et_email.getText().toString())){
            et_email.setError( errorString );
            return false;
        }
        return true;
    }

    private boolean checkPassword(String password) {
        String errorString=getString(R.string.error_password);
        if ("".equals(et_password.getText().toString())){
            et_password.setError( errorString );
            return false;
        }
        return true;
    }
}
