package com.daw.filmlist.model;

public class FilmG {


    String title;

    public FilmG(String title){
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
